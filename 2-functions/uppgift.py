def frame(str):
    stars = "*"*(len(str)+4)
    text = "{}{}{}"
    print(stars)
    print(text.format("* ", str, " *"))
    print(stars)

#frame("Välkommen till Python")
    

def triangle(num):
    stars = 1
    for line in range(0, num): #Each line of the triangle
        print("*"*stars)
        stars += 2

#triangle(20)

def stars(num):
    bred = num*21
    hbred = int(bred/2)
    lbred = hbred - 1
    hflag = "{}{}{}"
    for line in range(0,4):
        print(hflag.format("*"*hbred, " ","*"*lbred))
    

def flag(num):
    stars(num)
    print("")
    stars(num)
    
    
flag(1)
