def create_shopping_list():
    li = ["Kursliteratur", "Anteckningsblock", "Penna"]
    return li

def shopping_list(li):
    counter = 1
    for i in li:
        print(str(counter)+".", i) 
        counter +=1

def shopping_add(li):
    item = input("Vad ska läggas till i listan? ")
    li.append(item)

def shopping_remove(li):
    ind = int(input("Vilken sak vill du ta bort ur listan? "))
    num = ind - 1
    li.pop(num)
    

def shopping_edit(li):
    ind = int(input("Vilken sak vill du ändra på? "))
    old = li.pop(ind - 1)
    new = input('Vad ska det stå istället för \"'+ old + '\"?')
    li.insert(ind, new)
    
    
def shopping_editname(li):
    old = input("Vilken sak vill du ändra på? ")
    ind = li.index(old)
    li.remove(old)
    new = input('Vad ska det stå istället för \"'+ old + '\"')
    li.insert(ind, new)
