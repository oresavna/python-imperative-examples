def stars(num):
    """ Accepterar ett heltal som indata och returnerar 
    asterisker med den bred som heltalet anger """
    
    asterisker = "*"*num
    return asterisker

def frame(text):
    """ Accepterar en textsträng som indata och skriver ut
    textsträngen inramad med asterisker. """

    num = len(text) + 4
    print(stars(num))
    print("{}{}{}".format("* ", text, " *"))
    print(stars(num))
    

def triangle(num):
    """ Accepterar ett heltal som indata och skriver ut en triangel av asterisker
    med den höjd som heltalet anger."""
    
    bred = 1
    for line in range(0, num):
        print(stars(bred))
        bred +=2

        
def flag(num):
    """ Accepterar ett heltal som indata och skriver ut en flagga 
    av asterisker 21 gånger bredare än den storlek som heltalet anger."""
    
    bred = num*21
    if bred % 2 == 0:
        hbred = int(bred/2)
        lbred = hbred - 1
    else:
        hbred = int(bred/2)
        lbred = int(bred/2)

    # Skriver ut asterisker med "flag-stil" 
    for line in range(0, 4):
        print (stars(hbred), stars(lbred))
        
    print("")
    
    for line in range(0, 4):
        print (stars(hbred), stars(lbred))
        

