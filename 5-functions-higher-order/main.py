from high_order_functions import *


"""
# Execution Uppgift 5b - Företagsdatabas
db = [{'name': 'Jakob', 'position': 'assistant'}, {'name': 'Åke', 'position': 'assistant'}, {'name': 'Henrik', 'position': 'assistant'}, {'name': 'Ola', 'position': 'examiner'}]

#print(dbsearch(db, 'name','Ola'))
print(dbsearch(db, 'position', 'assistant'))


# Execution Uppgift 5c - Needles and Haystacks
haystack = 'Can you find the the needle in this haystack?'.split()
print(contains('needle', haystack))
print(contains('haystack', haystack))



# Execution Uppgift 5e - Funktioner som indata
def mirror(x): return x
def stars(n): return '*' * n
def hashtags(x): return '#' * x

print(generate_list(mirror, 4))
print(generate_list(stars, 5))
print(generate_list(hashtags, 6))


# Execution Uppgift 5f - Partial
def add(n, m): return n + m

add_five = partial(add, 5)
add_one = partial(add, 1)
print(add_five(3))
print(add_one(16))


# Execution Uppgift 5h - Compose
def multiply_five(n):
    return n * 5

def add_ten(x):
    return x + 10

def divide_two(x):
    return int(x/2)

composition = compose(multiply_five, add_ten)
print(composition(3))

another_composition = compose(add_ten, multiply_five)
print(another_composition(3))

division = compose(divide_two, add_ten)
print(division(16))

"""

# Execution Uppgift 5g - Filter the mapped result
process = make_filter_map(lambda x: x % 2 == 1, lambda x: x * x)
print((process(range(10))))


