def mirror(x): return x
def stars(n): return '*' * n

def generate_list(function, num):
    lista = [function(i) for i in range(1, num + 1)]
    return lista


print(generate_list(mirror, 4))
print(generate_list(stars, 5))

