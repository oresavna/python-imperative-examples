
def is_prime(n):
    # Filtrerar ut de värden som är relevanta
    for m in range(2, n):
        if n % m == 0: return False
    return True

#Med list comprehension:
list1 = [num for num in range(10) if is_prime(num)]
result1 = sum(list1)

#Med filter
iterator = filter(is_prime, range(10))
list2 = list(iterator)
result2 = sum(iterator)

#Med map
mapobj = map(is_prime, range(10))
list3 = list(mapobj)

"""
print(list1, result1)
print(iterator, list2, result2)
print(mapobj, list3)
"""


