def make_filter_map(Ff, Fm):
    """
    Accepts a filter-function(Ff) and a map function(Fm).
    Returns a function that takes a list as argument.
    Applies Fm to each element filtered by Ff.
    """
    return lambda list1: list(map(Fm, filter(Ff, list1)))


process = make_filter_map(lambda x: x % 2 == 1, lambda x: x * x)
print(process(range(10)))



