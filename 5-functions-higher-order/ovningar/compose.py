def multiply_five(n):
    return n * 5

def add_ten(x):
    return x + 10


def compose(F_a, F_b):
    return lambda x: F_a(F_b(x))
    

composition = compose(multiply_five, add_ten)
print(composition(3))

another = compose(add_ten, multiply_five)
print(another(3))



