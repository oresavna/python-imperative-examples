def add(n, m): return n + m

def partial(Fu, num):
    return lambda x: Fu(num, x)

add_five = partial(add, 5)
print(add_five(6))
    
    
