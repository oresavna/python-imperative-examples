def contains_old(element, lista):
    """
    Takes a list and finds out if a certain element is in the list
    """
    result = [word for word in lista if word == element]
    if not result:
        return False
    else:
        return True

def contains(element, lista):
    result = list(filter(lambda word: word == element, lista))
    if not result:
        return False
    else:
        return True
                

#print(filter(word == 'needle', word in haystack))

haystack = 'Can you find the the needle in this haystack?'.split()
print(contains('needle', haystack))
print(contains('haystack', haystack))



    
