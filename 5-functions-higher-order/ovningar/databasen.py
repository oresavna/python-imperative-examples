# Main function:

def dbsearch(db, search_field, search_value):
    """
    Accepts a database, a search field and the value the search field must have.
    Returns a list with the people that match the search.
    """
    search_result = [person for person in db if search_value == person[search_field]]
    return search_result


# Accessories and experiments:

def old_dbsearch(db, search_field, search_value):
    """
    First version of dbsearch. Longer.
    """
    result = []
    for person in db:
        # each person is a dictionary
        value = person[search_field]
        if value == search_value:
            result.append(person)
    return result


def create_db(db, name, position):
    """
    Little function that facilitated creating the db.
    """
    db.append({'name': name, 'position': position})
    return db


# EXECUTE

db = [{'name': 'Jakob', 'position': 'assistant'}, {'name': 'Åke', 'position': 'assistant'}, {'name': 'Henrik', 'position': 'assistant'}, {'name': 'Ola', 'position': 'examiner'}]

print(dbsearch(db, 'position','assistant'))
print(old_dbsearch(db, 'position', 'assistant'))
    
