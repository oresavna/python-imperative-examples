import os
import re


def ls():
    path = './'
    files = ""
    file_list = os.listdir(path)
    for file_ in file_list[:-1]: # Skriv inte ut en tom rad efter sista filen.
        files += file_ + "\n"
    files += file_list[-1]
    return files

def cd(command):
    path = command[3:]
    os.chdir(path)        

def pwd():
    return os.getcwd()

def cat(command):
    file_name = command[4:]
    file_content = ""
    for line in open(file_name):
        file_content += line
    return file_content
    
    


