import core
import re

done = False
while not done:
    command = input("command> ")
    if command == 'ls':
        print(core.ls())
        
    elif re.match(r'^cd ', command):
        try:
            core.cd(command)
        except FileNotFoundError:
            print("Directory not found. Try again.")
            
    elif command == 'pwd':
        print(core.pwd())
        
    elif command.startswith('cat '):
        try:
            file_content = core.cat(command)
            print(file_content)
        except FileNotFoundError:
            print("File not found. Try again.")
        
    elif command == 'exit':
        break
    
    else:
        print("Command not found. Try again.")
