from functools import reduce

#Gemensamn funktion
def interval_operation_long(num, op):
    if op == summa: result = 0
    if op == produkt: result = 1
    
    for x in range(1, num+1):
        result = op(result, x)
    return result

def interval_operation(num, Fx):
    numbers = [x for x in range(1, num+1)]
    return reduce(Fx, numbers)
    
    
#Uppgift 1a - lambdauttryck - summera
summa = lambda result, a: result + a

#Uppgift 1b - lambdauttryck - produkten
produkt = lambda result, n: result * n

print(interval_operation_long(5, summa))
print(interval_operation_long(5, produkt))

print(interval_operation(5, summa))
print(interval_operation(5, produkt))



"""
GAMLA FUNKTIONER
# Uppgift 1a -  Summera alla naturliga tal uppp till och med 512

num  = 1
result = 0
while num <= 512:
    result += num
    num += 1
print(result)

# Uppgift 1b - Produkten av alla positiva heltal upp till och med 512

num = 1
result = 1
while num <= 5:
    result *= num
    num += 1
print(result)
"""
