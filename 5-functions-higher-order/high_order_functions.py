
# Uppgift 5b - Företagsdatabasen
def dbsearch(db, search_field, search_value):
    """
    Accepts a database, a search field and the value the search field must have.
    Returns a list with the people that match the search.
    """
    search_result = [person for person in db if search_value == person[search_field]]
    return search_result

# Uppgift 5c - Needles and Haystacks
def contains(element, lista):
    result = list(filter(lambda word: word == element, lista))
    if not result:
        return False
    else:
        return True

# Uppgift 5e - Funktioner som indata
def generate_list(Fc, num):
    """
    Accepts a function Fc and an integer num.
    Fc is called num times.
    Returns a list with the results.
    """
    listx = [Fc(i) for i in range(1, num + 1)]
    return listx

# Uppgift 5f - Partial
def partial(Fu, num):
    """
    Takes a function Fu and a value num.
    Returns a new function that applies Fu on num and a new parameter x
    """
    return lambda x: Fu(num, x)


# Uppgift 5h - Compose
def compose(F_a, F_b):
    """
    Applies a composition of 2 functions F_a and F_b on a new parameter x
    """
    return lambda x: F_a(F_b(x))


# Uppgift 5g - Filter the mapped result
def make_filter_map(Ff, Fm):
    """
    Accepts a filter-function(Ff) and a map function(Fm).
    Returns a function that takes a list as argument.
    Applies Fm to each element filtered by Ff.
    """
    apply_map = partial(map, Fm)
    apply_filter = partial(filter, Ff)
    compose_map_filter = compose(apply_map, apply_filter)
    return lambda list1: list(compose_map_filter(list1))
