def create_stud(liu_id, grade, group, contact, courses, pnr):
    """
    returns a new student. Takes a liuId and grade
    as parameters
    """
    return {"id": str(liu_id), "grade": str(grade), "group": group, "contact": contact, "course": courses, "pnr": pnr}

def get_grade(student):
    return student["grade"]

def set_grade(student, grade):
    student["grade"] = grade
    return student

