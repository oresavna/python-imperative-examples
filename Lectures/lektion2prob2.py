
bad_words = ["Pontus", "Hej"]
censured = []
with open("text.txt", "r") as f:
    for line in f:
        words = line.split()
        for word in words:
            if word.rstrip("?!:;.*,") in bad_words:
                if len(word) != len(word.rstrip(",.!:")):
                    word = "*"*len(word.rstrip(",")) + word[-1]
                    censured.append(word)
                else:
                    word = "*"*len(word)
                    censured.append(word)
            else:
                censured.append(word.capitalize())
        censured.append("\n")
    censured.pop(-1)
    #print(censured)
        
with open("textOutput.txt", "w") as f:
    output_string = ""
    for s in censured:
        output_string += s
        if s != "\n":
            output_string += " "
    print(output_string, file=f)
"""

for list in lists:
    for element in list:
