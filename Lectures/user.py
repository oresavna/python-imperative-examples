import student

s1 = student.create_stud("ponha098", 5, "Avancerad", "pontus.haglund@liu.se", ["tpd001", "tpd002", "tdp003", "tdp004", "tdp005"], "00000000")
print(student.get_grade(s1))
print(s1["grade"]) # INTE RÄTT SÄTT!
print(s1) # INTE RELEVANT
s1 = student.set_grade(s1, "-")
print(student.get_grade(s1))
