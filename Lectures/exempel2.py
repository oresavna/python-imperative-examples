"""
lista = ["a", 2, 3]
#print(lista)
#print(lista[1])

for i in range(4):
    lista.append(i)

#print(lista)
#print("a" in lista)

sträng = "Hej jag heter Pontus"
print(sträng.find("hete", 2, 7))
"""
"""
import random
run = True
while(run):
    i = random.randint(1,6)
    print(i)
    if i == 6:
        run = False
"""
"""
import random
numbers = []
is_done = False
while not is_done:
    i = random.randint(1,6)
    numbers.append(i)
    if (5 in numbers) and (6 in numbers):
        is_done = True
print(numbers)
"""

import random

def is_numbers_in_list(x, y, l):
    return x in l and y in l

numbers = []
is_done = False
while not is_done:
    i = random.randint(1,6)
    numbers.append(i)
    is_done = is_numbers_in_list(5, 6, numbers)
    
print(numbers)
        
