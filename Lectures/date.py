"""
from datetime import date

print(date.today())
print(date.today().isoformat())
"""
#import pickle
"""
res = {'Kalle': 4, 'Anna': 5}
file_name = 'resultat'
with open(file_name, 'wb') as f:
    pickle.dump(res, f)
"""
#with open("resultat", "rb") as f:
#    print(pickle.load(f, encoding="utf-8"))

"""
import json

res = {'Kalle': 4, 'Anna': 5}
with open("resultat", "w") as f:
    print(json.dump(res,f, indent=2))
"""
"""
#res = #{'Kalle': 4, 'Anna': 5}
with open("resultat", "r") as f:
    res = json.load(f)
    res["Kalle"] = 6
    print(res)
"""

#import sys
#print(sys.argv)
#for arg in sys.argv:
#    print(arg)
#import subprocess
#subprocess.call(["ls"])

#import os
#print(os.listdir("/home/upp/TDP002/std"))


#import urllib.request
#url="http://www.ida.liu.se"
#with urllib.request.urlopen(url) as page:
#    for line in page
#            title = title[0]
#            print(line)
#        #break


import dis
dis.dis("""
print("hej")
5+5""")
