#! /usr/bin/env python3
import sys
import os

def main():
    if len(sys.argv) < 3:
        print("Needs a copyright source and at least 1 target file")
    elif len(sys.argv) >= 3:
        flags = flag_handler()
        copyright_file = sys.argv[1]
        target = sys.argv[2]
        

        if os.path.isdir(target):
            walks_directories(copyright_file, target, flags[0], flags[1])
        else:
            file_checker(target)
            copyright_to_file(copyright_file, target)



def walks_directories(copyright_file, target, flag_c = None, flag_u = None):

    for element in os.listdir(target):
        full_path =  os.path.join(target, element)
        
        if os.path.isdir(full_path):
            #If it's directory, apply recursive.
            walks_directories(copyright_file, full_path, flag_c, flag_u)

        elif os.path.isfile(full_path):
            #If it's file, check flags and apply function.
            if flag_c != None:
                if element.endswith(flag_c):
                    file_checker(full_path)
                    copyright_to_file(copyright_file, full_path)

                    if flag_u != None:
                        change_file_end(full_path, flag_u)                    

            else:
                file_checker(full_path)
                copyright_to_file(copyright_file, full_path)


def file_checker(file):
    print('Checking ...', file)

def file_applier(file):
    print('... Applying on:', file)

def change_file_end(full_path, flag_u):
    base = os.path.splitext(full_path)[0]
    os.rename(full_path, base + flag_u)
    
    
            
def flag_handler():
    flags = sys.argv[3:]

    flag_c = None
    flag_u = None
    if len(flags) == 2 and flags[0] == "-c":
        if flags[0] == "-c":
            flag_c = flags[1]

    elif len(flags) == 4:
        if flags[0] == "-c" and flags[2] == "-u":
            flag_c = flags[1]
            flag_u = flags[3]

    return [flag_c, flag_u]

    


def copyright_to_file(copyright_file, target_file):
    """
    Handles ONE file. 
    """
    result_text = add_text_between_tags(copyright_file, target_file)
    if result_text:
        write_to_file(result_text, target_file)


def add_text_between_tags(copyright_file, target_file):
    """
    Läser in en copyright_file och en target_file och kontrollerar 
    att markörerna BEGIN och END finns i target_file.

    Returnerar en lista med rader där texten som finns i source_file 
    infogas mellan markörerna i target_file.
    """
    with open(target_file) as target, open(copyright_file) as c:
        copyright_text = c.read()
        target_text = target.readlines()
        tags = False
        begin_copyright = False
        end_copyright = False
        result_text = []

        for line in target_text:

            if 'BEGIN COPYRIGHT' in line:
                begin_copyright = True
                result_text.append(line)
                result_text.append(copyright_text)
                
            elif 'END COPYRIGHT' in line:
                result_text.append(line)
                end_copyright = True
                
            elif not begin_copyright and not end_copyright:
                result_text.append(line)
                
            #print(i,line, begin_copyright, end_copyright)
  
            if begin_copyright and end_copyright:
                tags = True
                begin_copyright = False
                end_copyright = False

    if tags == True:
        return result_text
    else:
        return []

def write_to_file(result_text, target_file):
    file_applier(target_file)
    with open(target_file, 'w') as target:
        for line in result_text:
            target.write(line)



if __name__ == "__main__":
    main()

