from search import *
from sort import *


imdb = [{'title': 'Raise your voice', 'main actress': 'Hilary Duff', 'year': 1999, 'score': 6},
        {'title': 'True Lies', 'main actress': 'Jamie Lee Curtis', 'year': 1997, 'score': 7},
        {'title': 'Maleficent', 'main actress': 'Angelina Jolie', 'year': 2014, 'score': 9},
        {'title': 'Bourne Identity', 'main actress': 'Julia Stiles', 'year': 2000, 'score': 10}]


db2 = [('j', 'g'), ('a', 'u'), ('k', 'l'), ('o', 'i'), ('b', 's'), ('@', '.'), ('p', 's'), ('o', 'e')]

unsorted_list1 = [48, 33, 13, 99]
unsorted_list2 = ['b', 'd', 'a', 'c'] 

sorted_list1 = [1, 2, 3, 4]
sorted_list2 = ['a', 'b', 'c', 'd']



def test_linear_search(db):
    #print(linear_search(db, 10, lambda e: e['score']))
    print(linear_search(db, "1999", lambda movie: movie['year']))
    #print(linear_search(db, 10, lambda movie: movie['main actress']))
    #print(linear_search(db, 10))

def test_binary_search(db, Fx):
    insertion_sort(db, Fx) #Sorts first
    print(binary_search(db, 2014, Fx))
    #print(binary_search(db, 10, Fx))
    #print(binary_search(db, 7, Fx))


def test_insertion_sort(unsorted):
    db2 = [('j', 'g'), ('a', 'u'), ('k', 'l'), ('o', 'i'), ('b', 's'), ('@', '.'), ('p', 's'), ('o', 'e')]
    
    print(insertion_sort(unsorted, lambda e: e[0]))
    #print(insertion_sort(unsorted))

def test_quicksort(unsorted):
    #print(unsorted)
    print(quicksort(unsorted, lambda e: e[0]))
    #print(quicksort(unsorted, lambda e: e[1]))
    #print(quicksort(unsorted))



#test_linear_search(imdb)
#test_binary_search(imdb, lambda e: e['year'])
#test_insertion_sort(db2)
test_quicksort(db2)
