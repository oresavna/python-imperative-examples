def insertion_sort(List, sort_by = lambda e: e):
    """
    Tar en lista med element och en sort_by funktion
    """
 
    
    for i in range(1, len(List)):
        position = i
        element = List[i]
        subelement = sort_by(List[i])

        while position > 0 and subelement < sort_by(List[position - 1]):
            List[position] = List[position - 1]
            position -= 1
        List[position] = element
    return List
            
            
    
db2 = [('j', 'g'), ('a', 'u'), ('k', 'l'), ('o', 'i'), ('b', 's'), ('@', '.'), ('p', 's'), ('o', 'e')]
unsorted_list1 = [48, 33, 13, 99] 
print(insertion_sort(db2, lambda e: e[1]))
print(insertion_sort(unsorted_list1))




def simple_insertion_sort(List, sort_by = None):
    """
    Tar en lista med element och en sort_by funktion
    """  
    
    for i in range(1, len(List)):
        position = i
        element= List[i]       
        
        while position > 0 and element < List[position - 1]:
            List[position] = List[position - 1]
            position -= 1
        List[position] = element
    return List

            
#unsorted_list1 = [48, 33, 13, 99]        
#unsorted_list1 = [4, 2, 1, 3]
#unsorted_list1 = ['b', 'd', 'a', 'c']
#simple_insertion_sort(unsorted_list1)
  

def complex_insertion_sort(List, sort_by = None):

        
    for i in range(1, len(List)):

        position = i
        element = List[i]

        if sort_by == None:
            subelement = element
            previous_subelement = List[position - 1]
        else:
            subelement = sort_by(List[i])
            previous_subelement = sort_by(List[position - 1])

        while position > 0 and subelement < previous_subelement:
            List[position] = List[position - 1]
            position -= 1
            if sort_by == None:
                previous_subelement = List[position - 1]
            else:
                previous_subelement = sort_by(List[position - 1])
        List[position] = element
    print(List)
            
            
"""
db2 = [('j', 'g'), ('a', 'u'), ('k', 'l'), ('o', 'i'), ('b', 's'), ('@', '.'), ('p', 's'), ('o', 'e')]
print(db2)
complex_insertion_sort(db2, lambda e: e[0])
#complex_insertion_sort(db2)
complex_insertion_sort(unsorted_list1)
"""
