def linear_search(db, search_value, search_field = None):
    """
    Searches in a list for a value.
    Accepts a list and a search value
    Accepts an optional third argument to search by (a function).
    """
    result = []
    for element in db:
        if search_field != None:
            if search_value == search_field(element):
                return element
        else:
            #if search_value in element.values():
            #The above line is if the list is a list of dictionaries
            if str(search_value) in str(element):
                return element

            
def linear_search(db, search_value, search_field = lambda e: e):
    """
    Searches in a list for a value.
    Accepts a list and a search value
    Accepts an optional third argument to search by (a function).
    """
    result = []
    for element in db:
        if search_value in element.values():
        #if str(search_value) in str(element):
            return element
