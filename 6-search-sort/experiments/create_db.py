def create_film(title, main_actress, year, score):
    return {'title': title,
               'main actress': main_actress,
               'year': year,
               'score': score}

def create_db():
    db = []
    return db

def add_film(db, film):
    """
    Little function that facilitated creating the db.
    """
    db.append(film)
    return db


film1 = create_film('Raise your voice', 'Hilary Duff', 1999, 10)
film2 = create_film('True Lies', "Jamie Lee Curtis", 1997, 7)
film3 = create_film('Maleficent', 'Angelina Jolie', 2014, 6)
film4 = create_film('Bourne Identity', 'Julia Stiles', 2000, 8)

db = create_db()
add_film(db, film1)
add_film(db, film2)
add_film(db, film3)
add_film(db, film4)

print(db)
