def quicksort(sublist, sort_by = None):
    """
    Tar en lista med element och en sort_by funktion
    """
    small = []
    big = []
    if len(sublist) <= 1:
        return sublist
    else:
                   
        pivot = sublist.pop(0)
        middle = [pivot]

        if sort_by != None:
            pivot = sort_by(pivot)

        for item in sublist:
            
            if sort_by == None:
                subitem = item
            else:
                subitem = sort_by(item)
                
            if subitem < pivot:
                small.append(item)

            else:
                big.append(item)


        big = quicksort(big, sort_by)
        small = quicksort(small, sort_by)
                
    return small + middle + big


    
#unsorted_list1 = [9, 7, 1, 3, 2]
unsorted_list1 = ['b', 'd', 'a', 'c']

#print(quicksort(unsorted_list1))

db2 = [('j', 'g'), ('a', 'u'), ('k', 'l'), ('o', 'i'), ('b', 's'), ('@', '.'), ('p', 's'), ('o', 'e')]
print(db2)
print(quicksort(db2, lambda e: e[0]))
#print(quicksort(db2, lambda e: e[1]))


def partition_iter(List, pivot):
    partitioned = []
    small = []
    middle = [pivot]
    big = []
    
    for item in List:
        if item < pivot:
            small.append(item)
        else:
            big.append(item)
    partitioned.append(small)
    partitioned.append(middle)
    partitioned.append(big)
    return partitioned

def simple_quicksort(sublist, sort_by = None):
    """
    Tar en lista med element och en sort_by funktion
    """
    small = []
    big = []
    if len(sublist) <= 1:
        return sublist
    else:
        pivot = sublist.pop(0)
        middle = [pivot]
        for item in sublist:
            if item < pivot:
                small.append(item)
            else:
                big.append(item)

        big = quicksort(big)
        small = quicksort(small)
                
    return small + middle + big
