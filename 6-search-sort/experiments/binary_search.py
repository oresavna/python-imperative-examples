def binary_search(db, search_value, search_field = None):
    """
    Takes in a SORTED list.
    """
    
    low = 0
    high = len(db) - 1
    while low <= high:
        mid = (low + high)//2
        
        mid_element = db[mid] #retrieves movie in position mid
        print(mid, mid_element)
        if search_value > search_field(mid_element):
            low = mid + 1
        elif search_value < search_field(mid_element):
            high = mid - 1
        else:
            return mid
    return -1

def binary_search_simple(db, search_value, search_field = None):
    low = 0
    high = len(db)-1
    while low <= high:
        mid = (low + high)//2
        if search_value > db[mid]:
            low = mid + 1
        elif search_value < db[mid]:
            high = mid - 1
        else:
            return mid
    return -1
