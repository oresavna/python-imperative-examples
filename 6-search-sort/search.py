def linear_search(db, search_value, search_field = lambda e: e):
    """
    Tar en databas(lista) och söker efter en search_value
    Tar ett tredje argumentAccepts an optional third argument to search by (a function).
    Returns en lista med resultat.
    """
    result = []
    
    for element in db:
        if str(search_value) in str(search_field(element)):
            result.append(element)
    return result


def binary_search(db, search_value, search_field):
    """
    Tar en SORTERAD lista.
    """
    result = []
    low = 0
    high = len(db) - 1
    while low <= high:
        mid = (low + high)//2
        mid_element = db[mid] #retrieves movie in position mid
        if search_value > search_field(mid_element):
            low = mid + 1
        elif search_value < search_field(mid_element):
            high = mid - 1
        else:
            result.append(mid_element)
            break
    return result





        
    





