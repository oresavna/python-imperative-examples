def insertion_sort(List, sort_by = lambda e: e):
    """
    Tar en lista med element och en sort_by funktion
    Sorts enligt sort_by-funktionen.
    """
    
    for i in range(1, len(List)):
        position = i
        element = List[i]
        subelement = sort_by(List[i])

        while position > 0 and subelement < sort_by(List[position - 1]):
            List[position] = List[position - 1]
            position -= 1
        List[position] = element
    return List
            

def quicksort(current_list, sort_by = lambda e: e):
    """
    Tar en lista med element och en sort_by funktion.
    Sorts enligt sort_by-funktionen.
    """
    small = []
    big = []
    if len(current_list) <= 1:
        return current_list
    else:              
        pivot_list_element = current_list.pop(0)
        pivot = sort_by(pivot_list_element)
        middle = [pivot_list_element]

        #Större eller mindre an pivot?
        for list_element in current_list:
            element = sort_by(list_element)
            if element < pivot:
                small.append(list_element)
            else:
                big.append(list_element)

        # Rekursiva anrop
        big = quicksort(big, sort_by)
        small = quicksort(small, sort_by)
                
    return small + middle + big
