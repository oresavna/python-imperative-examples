from regnr import *


r1 = input_regnr('Mata in första registreringsnumret: ')
r2 = input_regnr('Mata in andra registreringsnumret: ')
ctr = -1
while not is_same(r1,r2):
	r1 = next(r1)
	ctr += 1
if ctr == -1: ctr = 0
print('Antal mellanliggande registreringsnummer:',ctr)
