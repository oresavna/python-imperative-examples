from functools import reduce
print('Du matade in din ',
      reduce(lambda s,w: s+w if s[-1]==' ' else s+w+'s ', 
             map(lambda w: w.rstrip('s').replace('mamma', 'mor').replace('pappa', 'far'), 
                 input('Mata in din släkting: ').split()
                 )
             ).rstrip('s '),
    '.', sep='')
