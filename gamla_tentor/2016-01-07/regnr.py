def create_regnr(t,s):
    return [t,s]

def is_same(r1,r2):
    return r1 == r2

def __incr(c):
    disallowed_chars = 'IQV'
    while True:
        c = chr(ord(c)+1)
        if c not in disallowed_chars:
            break
    return c

def input_regnr(p=''):
    line = input(p).strip()
    t,s = line.split()
    return create_regnr([c for c in t],int(s))

def next(r):
    t,s = r
    if s < 999:
        return create_regnr(t,s+1)
    s = 0
    for idx,c in reversed(list(enumerate(t))):
        if c != 'Z':
            t[idx]=__incr(c)
            break
        t[idx] = 'A'
    return create_regnr(t,s)


