parent = input('Mata in din släkting: ')
translated = []
for p in parent.split():
    if p.rstrip('s') == 'mamma':
        translated.append('mor')
    else:
        translated.append('far')

final = ''
for i in range(0, len(translated), 2):
    final += translated[i]
    if i+1 != len(translated):
        final += translated[i+1]
    if i < len(translated)-2:
        final += 's '


print('Du matade in din {}.'.format(final))

