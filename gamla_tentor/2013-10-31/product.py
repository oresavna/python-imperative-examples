from itertools import permutations

def to_vector(str):
    return [int(x) for x in str.split()]

v1=to_vector(input('Mata in v1: '))
v2=to_vector(input('Mata in v2: '))

min_prod=None
min_perm=[]
for perm in permutations(v1):
    prod=0
    for i in range(len(perm)):
        prod += perm[i]*v2[i]
    if min_prod is None or min_prod > prod:
        min_prod=prod
        min_perm=perm


def format_vector(vec):
    return ' '.join([str(i) for i in vec])

print('Minsta skalärprodukten: ({})*({})={}'.format(format_vector(min_perm), format_vector(v2), min_prod))
