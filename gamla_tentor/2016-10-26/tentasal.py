import math

rader = int(input("Mata in antal rader: "))
kolumner = int(input("Mata in antal kolumner: "))
tentander = int(input("Mata in antal tentander: "))

if tentander <= rader*kolumner:
    tentander = tentander
else:
    without_seat = tentander - rader*kolumner
    tentander = rader*kolumner
    
rows_with_extra = tentander % rader
ratio = tentander/rader
col_in_long_rows = math.ceil(ratio)
col_in_short_rows = math.floor(ratio)


print()


for line in range(rows_with_extra):
    print("[] "*col_in_long_rows + ">< "*(kolumner - col_in_long_rows))
for line in range(rader -  rows_with_extra):
    print("[] "*col_in_short_rows + ">< "*(kolumner - col_in_short_rows))

print()

if without_seat > 0:
    print(str(without_seat) + " studenter måste gå till annat rum.")
