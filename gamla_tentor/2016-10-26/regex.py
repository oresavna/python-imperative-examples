def match_first(s,r):
    return s[0] == r[0] or r[0] == '.'

def matches(s,r):
    if len(s) == 0:
        return len(r) == 0 or (len(r) == 2 and r[-1] in '*?')
    if len(r) == 1 and len(s) == 1:
        return match_first(s,r)
    elif len(r) == 1: return False
    elif r[1] == '?':
        return (match_first(s,r) and matches(s[1:], r[2:])) or matches(s, r[2:])
    elif r[1] == '*':
        return (match_first(s,r) and matches(s[1:], r)) or matches(s, r[2:])
    else:
        return match_first(s,r) and matches(s[1:], r[1:])

def check(s,r,e):
    print('Matching {} and {}'.format(s,r))
    m=matches(s,r)
    print('\t',end='')
    if (m and not e) or (not m and e):
        print('NOT ', end='')
    print('passed')

check('hejsan', '.eja?s*an', True)
check('kalle', 'anka', False)
check('kalle','.*', True)

