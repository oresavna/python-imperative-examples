import random

#Choosing word to draw:

def word_chooser():
    with open("./words.txt", "r") as f:
        words = f.read().splitlines()
        play_word = random.choice(words)
    return play_word


def is_letter_in_word(play_word, letter_guess):
    if letter_guess in play_word:
        return True
    else:
        return False   

def guessing_lines_handler(play_word, letters_guessed):
    guessing_lines = ""
    for letter in play_word:
        if letter in letters_guessed:
            guessing_lines += letter +" "
        else:
            guessing_lines += "_ "
    return guessing_lines    

def main():
    won = False
    print("Välkommen till hangman.\n")
    play_word = word_chooser()
    letters_guessed = ""
    guessed_word = ""
    guessing_lines = guessing_lines_handler(play_word, letters_guessed)
    #print(play_word)
    turns_left = 11
    
    while not won:

        print("Nuvarande ord: "+ str(guessing_lines))
        print("Gissade bokstäver: " +str(letters_guessed))
        print("Antal gissningar kvar: " + str(turns_left))
        letter_guess = input("Din gissning: ")

        if turns_left == 0:
            print("\n Du förlorade!")
            break
        
        if letter_guess == "":
            break
        
        elif letter_guess in letters_guessed:
            print("\nRedan använt "+ letter_guess + ".")
            letter_guess = input("Ny gissning: ")
            #  BUG! IT should take the input above and analyze it
            
        else:
            letters_guessed += letter_guess           
            if is_letter_in_word(play_word, letter_guess) == True:
                guessing_lines = guessing_lines_handler(play_word, letters_guessed)
                
                print("\nDet var rätt!")
                
                if guessing_lines.replace(" ","") == play_word:
                    print("Grattis du vann!")
                    won = True   
                    

                
            else:
                turns_left -= 1
                print("\nDet var fel!")
        
main()
