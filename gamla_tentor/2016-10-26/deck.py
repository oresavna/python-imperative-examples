from card import create_card

def create_deck():
    cards = []
    for suit in range(1,5):
        for value in range(1,14):
            cards.append(create_card(value, suit))
    return cards

def shuffle(deck):
    import random
    return random.shuffle(deck)

def take_card(deck):
    """ removes the top card from the deck. Returns None if the deck is empty """
    try:
        return deck.pop()
    except:
        return None
    
