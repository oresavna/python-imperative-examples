def matches(string, pattern):
    """ Checks if the pattern matches the string """
    # you are allowed to change the next line :)
    if len(string) == 0:
        if len(pattern) == 0 or (len(pattern) == 2 and pattern[1] in "?*"):
            return True
    elif len(string) == 1 and len(pattern) == 1:
        if pattern == string or pattern == ".":
            return True
    elif len(pattern) == 1:
        return False
    elif pattern[1] == "?":
        if matches(string, pattern[0]+pattern[2:]):
            return True
        elif matches(string, pattern[2:]):
            return True
    elif pattern[1] == "*":
        if matches(string[0], pattern[0]) and matches(string[1:], pattern):
            return True
        elif matches(string, pattern[2:]):
            return True
    elif matches(string[0], pattern[0]) and matches(string[1:], pattern[1:]):
        return True
        
        
    else:
        return False

def check(string, pattern, expected):
    print('Matching {} against {}'.format(string,pattern))
    m=matches(string,pattern)
    print('\t', end='')
    if (m and not expected) or (not m and expected):
        print('NOT ', end='')
    print('passed')

check('hej', 'hej', True)
check('hej', 'nej', False)
check('hejsan', '.eja?s*an', True)
check('kalle', 'anka', False)
check('kalle','.*', True)

# Egna testfall nedan

check('', '', True)
check('', 'a?', True)
check('', 'aaa?', False)
check('h', 'h', True)
check('h', 'a', False)
check('h', '.', True)
check('eli', 'e?li', True)
check('li', 'e?li', True)
