from card import *
from deck import *

d = create_deck()
shuffle(d)
cards = []
val = 0
while val < 17:
    c = take_card(d)
    cards.append(c)
    v = get_value(c)
    if v > 10:
        v = 10
    val += v

print(', '.join('{} {}'.format(get_suit(c), get_name(c)) for c in cards))
