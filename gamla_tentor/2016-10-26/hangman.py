import random
with open('words.txt') as f:
    chosen_word = random.choice(f.readlines()).strip()

guesses = 11
guessed_chars = ''
current_guess = ['_' for _ in range(len(chosen_word))]

print('Välkommen till Hangman\n')

while guesses > 0 and '_' in current_guess:
    print('Nuvarande ord:', *current_guess)
    print('Gissade bokstäver:', guessed_chars)
    print('Antal gissningar kvar:', guesses)
    guess = input('Din gissning: ')
    while guess in guessed_chars:
        print('\nRedan använt',guess)
        guess = input('Ny gissning: ')
    print()
    guessed_chars += guess
    if guess in chosen_word:
        print('Det var rätt!')
        for i,c in enumerate(chosen_word):
            if c == guess:
                current_guess[i] = c
    else:
        print('Det var fel!')
        guesses -= 1

if guesses == 0:
    print('Du förlorade')
else:
    print('Du vann!')

print('\nOrdet var', chosen_word)
