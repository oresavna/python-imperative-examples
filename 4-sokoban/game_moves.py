from create_objects import *

def move_player(board, direction):
    """
    Main function
    Handles the movement of a player on the board in the given direction.
    """
    mod_board = board.copy()
    player = where_is_player(board)
    goal_player = where_am_i_going(player[1], player[2], direction)
    future_player = create_player(goal_player[0], goal_player[1])
    future_player_on_dot = create_player_on_dot(goal_player[0], goal_player[1])
    space_floor = create_space(player[1], player[2])
    dot_floor = create_goal(player[1], player[2])

    # The movement depends on what object is in front of the player.
    object_in_front = which_object_is_here(mod_board, goal_player[0], goal_player[1])
    box_goal = where_am_i_going(future_player[1], future_player[2], direction)



    if can_player_move(board, future_player, object_in_front) == True:
        object_in_front_box = which_object_is_here(board, box_goal[0], box_goal[1])
        if object_in_front == 'space':
            replace_object_in_board(board, future_player)

            # Determines what is left behind the player after it moves.
            # Depending on whether the player is on a dot or not.
            if player[0] == 'player_on_dot':
                replace_object_in_board(board, dot_floor)
            else:
                replace_object_in_board(board, space_floor)

        elif object_in_front == 'goal':
            move_on_dots(board, player, direction, goal_player)

        elif object_in_front == 'box' and can_box_move(object_in_front_box) == True:
            push_box(mod_board, direction, future_player)
            replace_object_in_board(mod_board, future_player)


            if player[0] == 'player_on_dot':
                replace_object_in_board(mod_board, dot_floor)
            else:
                replace_object_in_board(mod_board, space_floor)


        elif object_in_front == 'box_on_dot' and can_box_move(object_in_front_box) == True:
            push_box(mod_board, direction, future_player)
            replace_object_in_board(mod_board, future_player_on_dot)


            if player[0] == 'player_on_dot':
                replace_object_in_board(mod_board, dot_floor)
            else:
                replace_object_in_board(mod_board, space_floor)
        else:
            return board
    else:
        return board
    



# ACCESSORY FUNCTIONS:

# ========================================================================================

# CHANGES THE BOARD

def push_box(board, direction, box_position):  # box_position = future_player
    """
    Handles the movement of the box as it's pushed.
    """
    box_goal = where_am_i_going(box_position[1], box_position[2], direction)
    box = create_box(box_goal[0], box_goal[1])
    box_on_dot = create_box_on_dot(box_goal[0], box_goal[1])
    object_in_front_box = which_object_is_here(board, box_goal[0], box_goal[1])
    
    if can_box_move(object_in_front_box) == False:
        return board
    
    else:
        if object_in_front_box == 'space':
            replace_object_in_board(board, box)

        elif object_in_front_box == 'goal':
            replace_object_in_board(board, box_on_dot)
        return board
    

def move_on_dots(board, player, direction, goal_player):
    future_player_on_dot = create_player_on_dot(goal_player[0], goal_player[1])
    dot_floor = create_goal(player[1], player[2])
    space_floor = create_space(player[1], player[2])
    
    replace_object_in_board(board, future_player_on_dot)
    if player[0] == 'player_on_dot':
        replace_object_in_board(board, dot_floor)
    else:
        replace_object_in_board(board, space_floor)
    return board

def replace_object_in_board(board, new_object):
    """
    Accepts a game board and a new_object in the form of ["box", x, y]
    Inserts the new object in the board, replacing the previous one.
    """
    
    mod_board = board.copy()
    #Identity of the object:
    obj = new_object[0] #For example "box"
    x = new_object[1]   
    y = new_object[2]
    board_row = mod_board.pop(y)
    board_row.pop(x) #Removes the old element x of row y
    char = which_character_is(obj) #Converts "wall" to "#"
    board_row.insert(x, char)
    mod_board.insert(y, board_row)
    return mod_board
       

# CHECKERS - DOES NOT CHANGE THE BOARD:

def where_is_player(board):
    """
    Accepts a game board.
    Finds the player in a board and returns it as an object depending on
    whether it's currently on an empty space or a dot space.
    """
    for row_num in range(0, len(board)):
        if '@' in board[row_num]:
            player_x = board[row_num].index('@')
            player_y = row_num
            return create_player(player_x, player_y)

        if '+' in board[row_num]:
            player_x = board[row_num].index('+')
            player_y = row_num
            return create_player_on_dot(player_x, player_y)


def can_player_move(board, future_player, object_in_front):
    """
    Checks the object in front of the player.
    Determines if a move is possible or not.
    """
    valid_objects = ['space', 'goal', 'box', 'box_on_dot']


    if (future_player[1]) >= board_limits(board)[0]:
        return False
    elif (future_player[2]) >= board_limits(board)[1]:
        return False
        
    if object_in_front in valid_objects:
        return True
    else:
        return False
    
def can_box_move(object_in_front_box):
    """
    Checks the object in front of the box.
    Determines if a move is possible or not.
    """
    if object_in_front_box == 'space' or object_in_front_box == 'goal':
        return True
    else:
        return False
    

def board_limits(board):
    lower_limit = len(board) - 1
    right_limit = len(board[0]) -1
    return right_limit, lower_limit


def which_object_is_here(board, x, y):
    """
    Accepts a game board, and coordinates x, y.
    Returns the object name, for example: "box"
    """
    board_mod = list(board) #To prevent changes being saved in the original list
    board_row = board_mod.pop(y)
    obj_in_place = board_row[x]
    obj = which_object_is(obj_in_place)
    return obj

def which_object_is(char):
    """
    Accepts a character like: #, o, S, @ ...
    Returns an object in text form "wall", "player".
    """
    character_to_object = {'#':'wall', '@':'player', 'S':'space', 'o':'box', '.':'goal', '+':'player_on_dot', '*':'box_on_dot'}
    obj = character_to_object[char]
    return obj
    
def which_character_is(obj):
    """
    Accepts an object in text form "wall", "player"
    Returns a character like: #, @
    """
    object_to_character = {'wall':'#', 'player':'@', 'space':'S', 'box':'o', 'goal':'.', 'player_on_dot':'+', 'box_on_dot':'*'}
    char = object_to_character[obj]
    return char
    

def where_am_i_going(x, y, direction):
    """
    Returns the coordinates in front of the player
    depending on the direction entered.
    """
    direction_dict = {'a':[x - 1, y], 'd':[x + 1, y], 'w':[x, y - 1], 's':[x, y + 1]}
    goal = direction_dict[direction]
    return goal
        

#Extra preparation
def extract_object_from_board(board, x, y):
    """
    Takes a list of lists (board) and coordinates.
    Shows the object in the form of ["box", x, y]
    """
    char = board[y][x]
    obj = which_object_is(char)
    complex_object = [obj, x, y]
    return complex_object

    
#Win conditions
def how_many_dots(board):
    """
    Finds how many dots are left (goals to complete)
    """
    dots_left = 0
    for row_num in board:
        for obj in row_num:
            if obj == '.':
                dots_left += 1
    return dots_left

def is_game_won(dots_left, board):
    if dots_left == 0 and where_is_player(board)[0] == 'player':
        won = True
    else:
        won = False
    return won
    
