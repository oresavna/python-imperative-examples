def print_row_string(row):
    """
    Takes in a list (row), displays it as ASCII string.
    """
    row_string = ""
    for e in row:
        if e == 'S':
            row_string += ' '
        else:
            row_string += e
    return row_string

def sokoban_display(board):
    """
    Takes in a list of lists (board), displays it as an ASCII level.
    """
    for row in board:
        print(print_row_string(row))

def maze_size(board):
    height = len(board)
    width = len(board[0])
    return (height, width)
    
        
