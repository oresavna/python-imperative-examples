def create_wall(x, y):
    wall = ['wall', x, y]
    return wall

def create_player(x, y):
    player = ['player', x, y]
    return player

def create_player_on_dot(x, y):
    player_on_dot = ['player_on_dot', x, y]
    return player_on_dot

def create_space(x, y):
    space = ['space', x, y]
    return space

def create_box(x, y):
    box = ['box', x, y]
    return box

def create_box_on_dot(x, y):
    box_on_dot = ['box_on_dot', x, y]
    return box_on_dot

def create_goal(x, y):
    goal = ['goal', x, y]
    return goal
