import load_maze
import display_maze
import game_moves

def main():
    #Level selection
    level_chooser = load_maze.level_chooser()
    level_number = input(level_chooser)

    #Loading the level
    current_level = load_maze.sokoban_load(int(level_number))

    #RUN THE GAME!
    won = False
    display_maze.sokoban_display(current_level)

    while not won:    
        #Ask for a direction
        direction = input("Move player with WASD or (e)xit: ")
        if direction == '':
            print("Try again:")
        elif direction in ['w', 'a', 's', 'd']:
            #Execute the move
            test_level = game_moves.move_player(current_level, direction)
        elif direction == 'e':
            print("Thanks for playing!")
            break
        else:
            print("That's not a valid move. Try again:")
            #Show the board
        display_maze.sokoban_display(current_level)

    

        #Show score
        dots_left = game_moves.how_many_dots(current_level)
        won = game_moves.is_game_won(dots_left, current_level)

    if won == True:
         print("Congrats! You won!")



if __name__ == '__main__':
    main()




