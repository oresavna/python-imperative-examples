import glob

# MENU GENERATOR

# =================================================================

def level_chooser():
    """
    Creates the menu text to print out for the player. Main function.
       * Creates list from files.
       * Converts list to a readable format
       * Formats the menu text
    """
    level_names = fetch_level_names()
    menu_text = "Which level do you want to play?\n"
    menu_number = 0
    for level in level_names:
        menu_number += 1 
        menu_text += str(menu_number) + ". " + str(level) + "\n"
    menu_text += "Choose: "
    return menu_text

def fetch_level_names():
    """
    Searches the levels directory for available Sokoban levels.
    Returns a list with level filenames.
    """
    path = './levels/*.sokoban'
    all_levels = glob.glob(path) #List
    all_levels = list(map(read_friendly_name, all_levels))
    all_levels.sort()
    return all_levels

def read_friendly_name(long_name):
    """
    Converts filenames to readable format.
    """
    friendly_name = long_name.replace('./levels/', '')
    friendly_name = friendly_name.replace('.sokoban', '')
    return friendly_name        
      
        

# LOADING A LEVEL

# =================================================================

def sokoban_load(level_number):
    """
    A sokoban level is read from a textfile (.sokoban).
    The entire level is converted to a matrix where: 
        * Each row is a sublist.
        * Each column is an object (wall, player, etc).
    It can now be manipulated for playing.
    """
    lista = level_load_file(level_number)
    width_board = width_of_level_board(lista)
    matrix = []
    for row in lista:
        row = add_tail(row, width_board)
        row = replace_spaces(row)
        matrix.append(row)
    return matrix


# INITIAL PROCESSING OF .SOKOBAN FILES.
# THESE FUNCTIONS ARE CALLED BY SOKOBAN_LOAD

def level_load_file(level_number):
    """
    loads each line of a .sokoban file into a list.
    It can probably be improved along with fetch_level_names to avoid 
    excessive editing.
    """
    level_names = fetch_level_names()
    current_level_name = level_names[level_number - 1] # To account for index beginning in 0
    path = './levels/'+str(current_level_name)+".sokoban"
    with open(path, 'r') as f:
        list_level = f.read().splitlines()
        return list_level

def choose_longest_element(lista):
    """
    Takes in a list and returns the longest element
    which would be the width of the game board.
    """
    maxi = max(lista, key=len)
    return maxi        
    
def width_of_level_board(lista):
    """
    Returns width of level board.
    Based on the longest row of the level.
    """
    width = len(choose_longest_element(lista))
    return width

def add_tail(row, width_board):
    """
    'completes' the spaces at the end of a row with 'S': "Space"
    """
    width_row = len(row)
    remaining = width_board - width_row
    row += 'S'*remaining
    return row

def replace_spaces(row):
    """
    Replace spaces in a row with 'S': "Space"
    Can probably be replaced with map or a list comprehension
    """
    row_items = []
    for e in row:
        #print(e)
        if e == ' ':
            row_items.append('S')
        else:
            row_items.append(e)
    return row_items

    








