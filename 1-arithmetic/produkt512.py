# Uppgift 1b - Produkten av alla positiva heltal upp till och med 512

num = 1
result = 1
while num <= 512:
    result *= num
    num += 1
print(result)
