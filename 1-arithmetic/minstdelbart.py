# Uppgift 1c - Minsta positiva heltalet som är jämt delbart med siffrorna 1 till och med 13

num = 1
divider = 1

while divider <= 13:
    if num % divider == 0:
        divider +=1
    else:
        num += 1
        divider = 1
print(num)
