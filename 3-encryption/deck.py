from card import create_card, get_suit, get_value
import random

def create_deck():
    """Generates a deck with 26 cards, including jokers."""
    game=[]

    for suit in range(1, 3):
        for value in range(1, 14):
            game.append(create_card(value, suit))
    game.append("joker A")
    game.append("joker B")
    return ["deck", game]

def convert_to_sequence(deck):
    deck_converted = []
    game_converted = []
    game = deck[1]
    for card in game:
        if card == "joker A" or card == "joker B":
            game_converted.append((27, 3))
        if get_suit(card) == 1:
            game_converted.append(card)
        elif get_suit(card) == 2:
            game_converted.append((get_value(card) + 13, 2))
    deck_converted.append(deck[0])
    deck_converted.append(game_converted)
    return deck_converted     

def shuffle_deck(deck, seed):
    """Shuffles the deck"""
    all_cards = deck[1]
    random.seed(seed)
    random.shuffle(all_cards)
    del deck[1]
    deck.append(all_cards)
    return deck

def insert_card(card, deck):
    """Inserts a specific card into the deck"""
    deck[1].insert(0, card)

def get_card(deck, num):
    """Accepterar en deck (som en lista) och returnerar ett kort (tuple)."""
    kort = deck[num]
    return kort

def move_joker_A(deck):
    """
    Algoritm steg 2 - Steget förändrar kortleken.
    Flyttar joker A 1 steg nedåt i kortleken.
    Om joker A är sista kortet flyttas det till den översta platsen.
    """
    all_cards = deck[1]
    if all_cards[-1] == "joker A":
        all_cards.remove("joker A")
        all_cards.insert(0, "joker A")
    else:
        index = all_cards.index("joker A")
        all_cards.remove("joker A")
        all_cards.insert(index + 1, "joker A")
    del deck[1]
    deck.append(all_cards)


def move_joker_B(deck):
    """
    Algoritm steg 3 - Steget förändrar kortleken.
    Flyttar joker B 2 steg nedåt i kortleken.
    Om joker B är näst sista kortetflyttas det till den översta platsen.
    Om joker B ligger på sista platsen flyttas den till näst översta platsen.
    """
    all_cards = deck[1]
    if all_cards[-2] == "joker B":
        all_cards.remove("joker B")
        all_cards.insert(0, "joker B")
    elif all_cards[-1] == "joker B":
        all_cards.remove("joker B")
        all_cards.insert(1, "joker B")
    else:
        index = all_cards.index("joker B")
        all_cards.remove("joker B")
        all_cards.insert(index + 2, "joker B")
    del deck[1]
    deck.append(all_cards)


def split_deck_A(deck):
    """Delar upp kortleken från första kortet fram till första jokern."""
    deck = deck[1]
    joker_A_index = deck.index("joker A")
    joker_B_index = deck.index("joker B")
    if joker_A_index < joker_B_index:
        return deck[:joker_A_index]
    else:
        return deck[:joker_B_index]


def split_deck_B(deck):
    """Delar upp kortleken från första jokern till och med sista jokern."""
    deck = deck[1]
    joker_A_index = deck.index("joker A")
    joker_B_index = deck.index("joker B")
    if joker_A_index < joker_B_index:
        return deck[joker_A_index:joker_B_index + 1]
    else:
        return deck[joker_B_index:joker_A_index + 1]

def split_deck_C(deck):
    """Delar upp kortleken från kortet efter sista jokern till sista kortet."""
    deck = deck[1]
    joker_A_index = deck.index("joker A")
    joker_B_index = deck.index("joker B")
    if joker_A_index < joker_B_index:
        return deck[joker_B_index + 1:]
    else:
        return deck[joker_A_index + 1:]


def rearrange_deck(deck):
    """
    Algoritm steg 4 - Steget förändrar kortleken.
    Ändrar ordningen på de olika delarna i den uppdelade kortleken.
    """
    part_A = split_deck_A(deck)
    part_B = split_deck_B(deck)
    part_C = split_deck_C(deck)
    del deck[1]
    deck.append(part_C + part_B + part_A)
    return deck


def get_last_value(deck):
    """Hämtar den understa kortsvärdet."""
    kort = get_card(deck[1], -1)
    value = get_value(kort)
    return value

def get_first_value(deck):
    """Hämtar den översta kortsvärdet."""
    kort = get_card(deck[1], 0)
    value = get_value(kort)
    return value

def topcards_to_bottom(value, deck):
    """Algoritm steg 5 - Steget förändrar kortleken.
    Flyttar lika många kort från övre delen av kortleken som värdet på det
    understa kortet och sätt in dessa precis ovanför det understa kortet.
    """
    for i in range(0, value):
        i = deck.pop(0)
        deck.insert(-1, i)
    return deck
