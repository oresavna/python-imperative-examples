"""
Handles the text for encrypting
"""

from card import create_card, get_suit, get_value


def onlyletters(text):
    """
    Takes away anything that is not a letter A-Z or a-z
    And converts lowercase to uppercase
    """
    letters = ""
    for letter in text:
        letter = letter.upper()
        if ord(letter) in range(65, 91):
            letters += letter
    return letters

def msg_letters_numbers(letters):
    numbers = []
    for i in letters:
        num = ord(i)
        num -= 64
        numbers.append(num)
    return numbers

def msg_numbers_letters(numbers):
    letters = ""
    for i in numbers:
        let = i + 64
        let = chr(let)

        letters += let
    return letters

def value_to_letter(value):
    """
    Matchar en korts värde till en bokstav enligt 1 = A, 2 = B
    """
    if value == 0:
        return None
    else:
        ascii_friendly = value + 64  # 65 = A, 66 = B
        return chr(ascii_friendly)
