from text_convert import *
from deck import *

def solitaire_encrypt(msg, deck):
    """
    Tar ett meddelande och en kortlek.
    Returnerar ett encrypterat meddelande.
    """
    msg = onlyletters(msg) #Bara A-Z, alla versaler.
    # Generera en nyckelfras med samma längd som meddelandet
    key_phrase = solitaire_keystream(msg, deck)

    # Förvandla både originella meddelandet och nyckelfrasen till tal
    msg_numbers = msg_letters_numbers(msg)
    key_numbers = msg_letters_numbers(key_phrase)
    
    # Addera msg_numbers och key_numbers - dra bort 26 om summan är högre än 26.
    encrypted_numbers = []
    for msg_num, key_num in zip(msg_numbers, key_numbers):
        sum_num_key = msg_num + key_num
        if sum_num_key > 26:
            sum_num_key -= 26
        encrypted_numbers.append(sum_num_key)
   
    #Steg 6. Konvertera talen i steg 5 till bokstäver.
    encrypted_msg = msg_numbers_letters(encrypted_numbers)
    return encrypted_msg


def solitaire_decrypt(encrypted, deck):
    """
    Tar ett encrypterat meddelande och en kortlek.
    Returnerar ett decrypterat meddelande.
    """
    # Konvertera bokstäverna tillbaka till tal
    key_phrase = solitaire_keystream(encrypted, deck)
    encrypted_msg = encrypted
    encrypted_numbers = msg_letters_numbers(encrypted_msg)
    key_numbers = msg_letters_numbers(key_phrase)
    msg_numbers = []
    for enc_num, key_num in zip(encrypted_numbers, key_numbers):
        msg_num = enc_num - key_num
        if msg_num < 1:
            msg_num += 26
        msg_numbers.append(msg_num)
    decrypted_msg = msg_numbers_letters(msg_numbers)
    return decrypted_msg


    

def solitaire_keystream(msg, deck):
    """
    Returnerar en bokstav utöver de flesta funktioner i denna modul.
    """
    counter = 1
    key_phrase = ""
    print(deck)
    
    while counter <= len(msg):
        move_joker_A(deck) # 1 steg nedan
        move_joker_B(deck) # 2 steg nedan

        deck = rearrange_deck(deck) # 3 delar ABC till CBA
        deck_converted = convert_to_sequence(deck)
        
        value_last_card = get_last_value(deck_converted)
        deck_converted = topcards_to_bottom(value_last_card, deck_converted)
        # Flytta x topkort --> ovanför det understa kortet.

        value_first_card = get_first_value(deck_converted) #värdet x på det översta kortet.
        kort = get_card(deck_converted[1], value_first_card) # välja kortet efter x kort uppifrån.
        
        # Kortetsvärdet bestämmer aktuella bokstaven till nyckelfrasen.
        value = get_value(kort)
        letter = value_to_letter(value)

        key_phrase += letter
        counter += 1
    return key_phrase
