
def create_card(val, suit):
    """
    Returns a tuple
    """
    kort = val, suit
    return kort

def get_suit(kort):
    """Accepterar ett kort (tuple). Returnerar kortetsfärgen."""
    if type(kort) == tuple:
        return kort[1] # 0 är den andra elementet i paret (value, suit)
    else:
        return "Joker"

def get_value(kort):
    """Takes a card as an argument (a tuple) and returns the cards value"""
    if type(kort) == tuple: #If the card is a tuple, it's not a joker
        return kort[0]
    else:
        return 0


