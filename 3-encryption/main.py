from deck import *
from encrypt import *

# Create a deck!
deck = create_deck()


# Choose a seed and shuffle the deck!
seed = 100
deck = shuffle_deck(deck, seed)

# Enter the message you want to encrypt!
msg = "DAJEWAD #ASSS "

# Encrypting the message!
encrypted = solitaire_encrypt(msg, deck)

print("The encrypted message is: ")
print(encrypted)

#Recreating the deck!
deck = create_deck()
deck = shuffle_deck(deck, seed)

# Decrypting the message!
decrypted = solitaire_decrypt(encrypted, deck)

print("The decrypted message is: ")
print(decrypted)



