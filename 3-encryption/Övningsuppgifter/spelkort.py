"""Funktioner som hanterar ett spelkort och kan användas av olika kortspel."""

def get_card(deck, num):
    """Accepterar en deck (som en lista) och returnerar ett kort (tuple)."""
    kort = deck[num]
    return kort

def get_value(kort):
    """Accepterar ett kort (tuple). Returnerar kortetsvärdet."""
    if type(kort) == tuple: # Kontrollerar att det inte är en joker
        return kort[0]
    else:
        return 0

def get_suit(kort):
    """Accepterar ett kort (tuple). Returnerar kortetsfärgen."""
    return kort[1] # 0 är den andra elementet i paret (value, suit)

def create_card(val, suit):
    """
    Accepterar två parametrar:
      * Valör (val) Ett heltal mellan 1 och 13
      * Färg (suit) Ett heltal mellan 1 och 4:
    Skapar ett kort
    """
    kort = val, suit
    return kort
