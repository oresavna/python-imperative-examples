"""
Funktioner som ändrar en kortlek för att skapa en nyckelfras

"""

import random
import kortlek
import spelkort

joker_A = 'joker A'
joker_B = 'joker B'

def move_joker_A(deck):

    """
    Algoritm steg 2 - Steget förändrar kortleken.
    Flyttar joker A 1 steg nedåt i kortleken.
    Om joker A är sista kortet flyttas det till den översta platsen.
    """
    all_cards = deck[1]
    if all_cards[-1] == joker_A:
        all_cards.remove(joker_A)
        all_cards.insert(0, joker_A)
    else:
        index = all_cards.index(joker_A)
        all_cards.remove(joker_A)
        all_cards.insert(index + 1, joker_A)
    del deck[1]
    deck.append(all_cards)


def move_joker_B(deck):
    """
    Algoritm steg 3 - Steget förändrar kortleken.
    Flyttar joker B 2 steg nedåt i kortleken.
    Om joker B är näst sista kortetflyttas det till den översta platsen.
    Om joker B ligger på sista platsen flyttas den till näst översta platsen.
    """
    all_cards = deck[1]
    if all_cards[-2] == joker_B:
        all_cards.remove(joker_B)
        all_cards.insert(0, joker_B)
    elif all_cards[-1] == joker_B:
        all_cards.remove(joker_B)
        all_cards.insert(1, joker_B)
    else:
        index = all_cards.index(joker_B)
        all_cards.remove(joker_B)
        all_cards.insert(index + 2, joker_B)
    del deck[1]
    deck.append(all_cards)


def split_deck_A(deck):
    """Delar upp kortleken från första kortet fram till första jokern."""
    deck = deck[1]
    joker_A_index = deck.index(joker_A)
    joker_B_index = deck.index(joker_B)
    if joker_A_index < joker_B_index:
        return deck[:joker_A_index]
    else:
        return deck[:joker_B_index]


def split_deck_B(deck):
    """Delar upp kortleken från första jokern till och med sista jokern."""
    deck = deck[1]
    joker_A_index = deck.index(joker_A)
    joker_B_index = deck.index(joker_B)
    if joker_A_index < joker_B_index:
        return deck[joker_A_index:joker_B_index + 1]
    else:
        return deck[joker_B_index:joker_A_index + 1]

def split_deck_C(deck):
    """Delar upp kortleken från kortet efter sista jokern till sista kortet."""
    deck = deck[1]
    joker_A_index = deck.index(joker_A)
    joker_B_index = deck.index(joker_B)
    if joker_A_index < joker_B_index:
        return deck[joker_B_index + 1:]
    else:
        return deck[joker_A_index + 1:]


def rearranged_deck(deck):
    """
    Algoritm steg 4 - Steget förändrar kortleken.
    Ändrar ordningen på de olika delarna i den uppdelade kortleken.
    """
    part_A = split_deck_A(deck)
    part_B = split_deck_B(deck)
    part_C = split_deck_C(deck)
    del deck[1]
    deck.append(part_C + part_B + part_A)
    return deck


def get_last_value(deck):
    """Hämtar den understa kortsvärdet."""
    kort = spelkort.get_card(deck[1], -1)
    value = spelkort.get_value(kort)
    return value

def get_first_value(deck):
    """Hämtar den översta kortsvärdet."""
    kort = spelkort.get_card(deck[1], 0)
    value = spelkort.get_value(kort)
    return value


def move_cards(value, deck):
    """Algoritm steg 5 - Steget förändrar kortleken.
    Flyttar lika många kort från övre delen av kortleken som värdet på det
    understa kortet och sätt in dessa precis ovanför det understa kortet.
    """
    for i in range(0, value):
        i = deck.pop(0)
        deck.insert(-1, i)
    return deck


def nyckel(value):
    """
    Matchar en korts värde till en bokstav enligt 1 = A, 2 = B

    """
    if value == 0:
        return None
    else:
        ascii_friendly = value + 64  # 65 = A, 66 = B
        return chr(ascii_friendly)

def solitaire_keystream_steg(length, deck):
    """
    Returnerar en bokstav utöver de flesta funktioner i denna modul.
    """

    counter = 1
    nyckelfras = ""

    # Steg 1. Lägga till 2 jokers till kortleken och blanda hela kortleken.
    kortlek.insert_card(joker_A, deck)
    kortlek.insert_card(joker_B, deck)
    kortlek.shuffle_deck(deck)
    while counter <= length:
        # Steg 2. Flytta joker A ner ett steg i kortleken.
        move_joker_A(deck)
        # Steg 3. Flytta joker B ner två steg i kortleken.
        move_joker_B(deck)
        # Steg 4. Dela kortleken i tre delar A, B, C. Flytta delarna till C, B, A
        deck = rearranged_deck(deck)
        # Spara värdet på det understa kortet
        value = get_last_value(deck)
        # Steg 5. Flytta <value> kort från övre delen av leken och sätta dem ovanför det understa kortet.
        deck = move_cards(value, deck)
        # Steg 6. Det översta kortets värde är:
        value = get_first_value(deck)

        # Räkna lika många kort uppifrån som värdet på det översta kortet.
        # Välj kortet direkt efter dessa kort.
        kort = spelkort.get_card(deck[1], value)
        # Det är värdet för nästa bokstav i nyckelfrasen.
        nyckel_num = spelkort.get_value(kort)
        letter = nyckel(nyckel_num)
        if letter == None:
            continue
        else:
            nyckelfras += letter
            counter += 1
    print(nyckelfras)

"""
solitaire_deck = kortlek.create_deck()
solitaire_keystream_steg(30, solitaire_deck)
"""
