"""
Funktioner som hanterar ett deck generellt och kan användas av solitaire och andra kortlek.

"""

import random
import spelkort

def create_deck():
    """
    Generates a full deck with 52 cards.
    """

    game=[]
    for suit in range(1, 5):
        for value in range(1, 14):
            card = value, suit
            game.append(card)


    return ["deck", game]



def pick_card(deck):
    """
    Takes the top card from the deck created with  create_deck()
    """
    return deck[1].pop(0)

def insert_card_num(val, suit, deck):
    """
    Inserts a card as the top card of the deck created with create_deck()
    """
    create = spelkort.create_card(val, suit)
    card = create["value"], create["suit"]
    deck[1].insert(0, card)

def insert_card(card, deck):
    deck[1].insert(0, card)


def shuffle_deck(deck):
    random.shuffle(deck[1])
